oauth2-resource-server-example
==============================

[![pipeline status](https://gitlab.com/madarche/oauth2-resource-server-example/badges/master/pipeline.svg)](https://gitlab.com/madarche/oauth2-resource-server-example/-/commits/master)
[![coverage report](https://gitlab.com/madarche/oauth2-resource-server-example/badges/master/coverage.svg)](https://gitlab.com/madarche/oauth2-resource-server-example/-/commits/master)

OAuth2 Resource Server (RS) example


Usage
-----

First, write the `config.tom` config file:

```shellsession
cp config.toml.example config.toml
vim config.toml
```

Then, install the needed packages and start the application:

```shellsession
npm ci
npm start
```


API
---

The API is defined in the
[openapi-schema.yml](src/openapi-schema.yml)
file.

